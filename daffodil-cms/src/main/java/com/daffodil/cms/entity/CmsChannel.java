package com.daffodil.cms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Dict;
import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.Ztree;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 栏目（站点）表
 * @author yweijian
 * @date 2020年10月23日
 * @version 1.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="cms_channel")
public class CmsChannel extends Ztree<String> {
	private static final long serialVersionUID = -6444285890926345310L;

	/** 栏目编号 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "channel_id")
	private String id;
	
	/** 站点ID */
	@Column(name = "site_id")
	@Hql(type = Logical.EQ)
	private String siteId;
	
	/** 站点名称（不作为字段） */
	@Transient
	private String siteName;

	/** 父栏目ID */
	@Column(name = "parent_id")
	@Hql(type = Logical.EQ)
	private String parentId;

	/** 祖级列表 */
	@Column(name = "ancestors")
	private String ancestors;

	/** 栏目名称 */
	@Column(name = "channel_name")
	@NotBlank(message = "栏目名称不能为空")
	@Size(min = 0, max = 30, message = "栏目名称长度不能超过30个字符")
	@Hql(type = Logical.LIKE)
	private String channelName;

	/** 显示顺序 */
	@Column(name = "order_num")
	private Long orderNum;

	/** 栏目状态（0正常 1停用 2删除） */
	@Column(name = "status")
	@Dict(value = "sys_data_status")
	@Hql(type = Logical.EQ)
	private String status;
	
	/** 创建者 */
	@Column(name="create_by")
	@Hql(type = Logical.LIKE)
	private String createBy;

	/** 创建时间 */
	@Column(name="create_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/** 更新者 */
	@Column(name="update_by")
	@Hql(type = Logical.LIKE)
	private String updateBy;

	/** 更新时间 */
	@Column(name="update_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

	/** 备注 */
	@Column(name="remark")
	@Hql(type = Logical.LIKE)
	private String remark;
	
}
