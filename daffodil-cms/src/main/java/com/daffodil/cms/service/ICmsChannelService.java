package com.daffodil.cms.service;

import java.util.List;

import com.daffodil.cms.entity.CmsChannel;
import com.daffodil.core.entity.Query;

/**
 * 栏目（站点）管理
 * @author yweijian
 * @date 2020年10月23日
 * @version 1.0
 * @description
 */
public interface ICmsChannelService {
	
	/**
	 * 根据查询条件查询栏目（站点）
	 * @param query
	 * @return
	 */
	public List<CmsChannel> selectChannelList(Query<CmsChannel> query);

	/**
	 * 按树结构查询栏目（站点）
	 * @param channel
	 * @return
	 */
	public List<CmsChannel> channelTreeData(Query<CmsChannel> query);

	/**
	 * 删除栏目（站点）管理信息
	 * @param channelId
	 */
	public void deleteChannelById(String channelId);

	/**
	 * 新增保存栏目（站点）信息
	 * @param channel
	 */
	public void insertChannel(CmsChannel channel);

	/**
	 * 修改保存栏目（站点）信息
	 * @param channel
	 */
	public void updateChannel(CmsChannel channel);

	/**
	 * 根据栏目（站点）ID查询信息
	 * @param channelId
	 * @return
	 */
	public CmsChannel selectChannelById(String channelId);

	/**
	 * 校验栏目（站点）名称是否唯一
	 * @param channel
	 * @return
	 */
	public boolean checkChannelNameUnique(CmsChannel channel);

	/**
	 * 批量新增栏目
	 * @param parentId 父栏目ID
	 * @param channels 栏目集合
	 */
	public void batchAddChannel(String parentId, String channels);
}
