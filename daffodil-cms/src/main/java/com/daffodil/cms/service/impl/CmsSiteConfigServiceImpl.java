package com.daffodil.cms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.cms.entity.CmsSiteConfig;
import com.daffodil.cms.service.ICmsSiteConfigService;
import com.daffodil.core.dao.JpaDao;

/**
 * 
 * @author yweijian
 * @date 2020年10月29日
 * @version 1.0
 * @description
 */
@Service
public class CmsSiteConfigServiceImpl implements ICmsSiteConfigService {
	
	@Autowired
	private JpaDao<String> jpaDao;
	
	@Override
	public CmsSiteConfig selectSiteConfigBySiteId(String siteId) {
		return jpaDao.find("from CmsSiteConfig where siteId = ?", siteId, CmsSiteConfig.class);
	}

	@Override
	@Transactional
	public void insertSiteConfig(CmsSiteConfig config) {
		jpaDao.save(config);
	}

	@Override
	@Transactional
	public void updateSiteConfig(CmsSiteConfig config) {
		jpaDao.update(config);
	}

}
