package com.daffodil.system.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daffodil.core.annotation.Log;
import com.daffodil.core.annotation.Log.BusinessType;
import com.daffodil.core.controller.BaseController;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.system.entity.SysUser;
import com.daffodil.system.service.ISysUserService;
import com.daffodil.framework.shiro.util.ShiroUtils;
import com.daffodil.util.StringUtils;
import com.daffodil.util.sm.SM2Utils;

/**
 * 个人信息控制层
 * 
 * @author yweijian
 * @date 2019年8月18日
 * @version 1.0
 */
@Controller
@RequestMapping("/system/user/profile")
public class SysProfileController extends BaseController {

	private String prefix = "system/user/profile";
	
	/**
	 * 私钥
	 */
	@Value("${sm.private.key}")
	private String privateKey;

	@Autowired
	private ISysUserService userService;

	/**
	 * 个人信息
	 */
	@GetMapping()
	public String profile(ModelMap modelMap) {
		SysUser user = ShiroUtils.getSysUser();
		modelMap.put("user", user);
		modelMap.put("roleGroup", userService.selectUserRoleGroup(user.getId()));
		modelMap.put("postGroup", userService.selectUserPostGroup(user.getId()));
		return prefix + "/profile";
	}

	@GetMapping("/checkPassword")
	@ResponseBody
	public boolean checkPassword(String oldPassword) {
		SysUser user = ShiroUtils.getSysUser();
		oldPassword = SM2Utils.decryptData(privateKey, oldPassword);
		oldPassword = ShiroUtils.encryptPassword(user.getLoginName(), oldPassword, user.getSalt());
		if (user.getPassword().equals(oldPassword)) {
			return true;
		}
		return false;
	}

	@GetMapping("/resetPwd")
	public String resetPwd(ModelMap modelMap) {
		SysUser user = ShiroUtils.getSysUser();
		modelMap.put("user", userService.selectUserById(user.getId()));
		return prefix + "/resetPwd";
	}

	@Log(title = "重置密码", businessType = BusinessType.UPDATE)
	@PostMapping("/resetPwd")
	@ResponseBody
	public JsonResult resetPwd(String oldPassword, String newPassword) {
		SysUser user = ShiroUtils.getSysUser();
		oldPassword = SM2Utils.decryptData(privateKey, oldPassword);
		oldPassword = ShiroUtils.encryptPassword(user.getLoginName(), oldPassword, user.getSalt());
		if (StringUtils.isNotEmpty(newPassword) && user.getPassword().equals(oldPassword)) {
			user.setPassword(newPassword);
			userService.resetUserPwd(user);
			ShiroUtils.setSysUser(userService.selectUserById(user.getId()));
			return JsonResult.success();
		} else {
			return JsonResult.warn("修改密码失败，原始密码错误");
		}
	}

	/**
	 * 修改用户
	 */
	@GetMapping("/edit")
	public String edit(ModelMap modelMap) {
		SysUser user = ShiroUtils.getSysUser();
		modelMap.put("user", userService.selectUserById(user.getId()));
		return prefix + "/edit";
	}

	/**
	 * 修改头像
	 */
	@GetMapping("/avatar")
	public String avatar(ModelMap modelMap) {
		SysUser user = ShiroUtils.getSysUser();
		modelMap.put("user", userService.selectUserById(user.getId()));
		return prefix + "/avatar";
	}

	/**
	 * 修改用户
	 */
	@Log(title = "个人信息", businessType = BusinessType.UPDATE)
	@PostMapping("/update")
	@ResponseBody
	public JsonResult update(SysUser user) {
		SysUser currentUser = ShiroUtils.getSysUser();
		currentUser.setUserName(user.getUserName());
		currentUser.setEmail(user.getEmail());
		currentUser.setPhone(user.getPhone());
		currentUser.setSex(user.getSex());
		currentUser.setAvatar(user.getAvatar());
		userService.updateUserInfo(currentUser);
		ShiroUtils.setSysUser(userService.selectUserById(currentUser.getId()));
		return JsonResult.success();
	}

}
