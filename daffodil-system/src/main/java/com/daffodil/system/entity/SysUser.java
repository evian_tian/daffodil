package com.daffodil.system.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Dict;
import com.daffodil.core.annotation.Excel;
import com.daffodil.core.annotation.Excel.Type;
import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户表
 * 
 * @author yweijian
 * @date 2019年12月12日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_user")
public class SysUser extends BaseEntity<String> {
	private static final long serialVersionUID = 2563124405284230396L;

	/** 用户ID */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "user_id")
	@Excel(name = "用户编号", type = Type.EXPORT)
	private String id;
	
	/** 是否是管理员 (Y=是,N=否)*/
	@Column(name = "is_admin")
	@Hql(type = Logical.EQ)
	private String isAdmin;

	/** 部门ID */
	@Column(name = "dept_id")
	@Excel(name = "部门编号", type = Type.IMPORT)
	private String deptId;

	/** 登录名称 */
	@Column(name = "login_name")
	@Excel(name = "登录名称")
	@NotBlank(message = "登录账号不能为空")
	@Size(min = 0, max = 30, message = "登录账号长度不能超过30个字符")
	@Hql(type = Logical.LIKE)
	private String loginName;

	/** 用户名称 */
	@Column(name = "user_name")
	@Excel(name = "用户名称")
	@Size(min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
	@Hql(type = Logical.LIKE)
	private String userName;

	/** 用户邮箱 */
	@Column(name = "email")
	@Excel(name = "用户邮箱")
	@Email(message = "邮箱格式不正确")
	@Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
	@Hql(type = Logical.LIKE)
	private String email;

	/** 手机号码 */
	@Column(name = "phone")
	@Excel(name = "手机号码")
	@Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
	@Hql(type = Logical.LIKE)
	private String phone;

	/** 用户性别 */
	@Column(name = "sex")
	@Dict(value = "sys_user_sex")
	@Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
	@Hql(type = Logical.EQ)
	private String sex;

	/** 用户头像 */
	@Column(name = "avatar")
	private String avatar;

	/** 密码 */
	@Column(name = "password")
	private String password;

	/** 盐加密 */
	@Column(name = "salt")
	private String salt;

	/** 帐号状态（0正常 1停用 2删除） */
	@Column(name = "status")
	@Dict(value = "sys_data_status")
	@Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用,2=删除")
	@Hql(type = Logical.EQ)
	private String status;

	/** 最后登陆IP */
	@Column(name = "login_ip")
	@Excel(name = "最后登陆IP", type = Type.EXPORT)
	@Hql(type = Logical.LIKE)
	private String loginIp;

	/** 最后登陆时间 */
	@Column(name = "login_time")
	@Excel(name = "最后登陆时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date loginTime;

	/** 创建者 */
	@Column(name="create_by")
	@Hql(type = Logical.LIKE)
	private String createBy;

	/** 创建时间 */
	@Column(name="create_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/** 更新者 */
	@Column(name="update_by")
	@Hql(type = Logical.LIKE)
	private String updateBy;

	/** 更新时间 */
	@Column(name="update_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

	/** 备注 */
	@Column(name="remark")
	@Hql(type = Logical.LIKE)
	private String remark;
	
	/** 部门对象 */
	@Transient
	private SysDept dept;

	/** 角色对象 */
	@Transient
	private List<SysRole> roles;

	/** 角色组id */
	@Transient
	private String[] roleIds;
	
	/** 岗位对象 */
	@Transient
	private List<SysPost> posts;

	/** 岗位组id */
	@Transient
	private String[] postIds;

	public SysUser() {
        super();
    }
	
    public SysUser(String id, String isAdmin, String deptId,String loginName, String userName,String email, String phone, 
            String sex, String avatar, String status, String loginIp, Date loginTime, String createBy, Date createTime, String updateBy,
            Date updateTime, String remark) {
        super();
        this.id = id;
        this.isAdmin = isAdmin;
        this.deptId = deptId;
        this.loginName = loginName;
        this.userName = userName;
        this.email = email;
        this.phone = phone;
        this.sex = sex;
        this.avatar = avatar;
        this.status = status;
        this.loginIp = loginIp;
        this.loginTime = loginTime;
        this.createBy = createBy;
        this.createTime = createTime;
        this.updateBy = updateBy;
        this.updateTime = updateTime;
        this.remark = remark;
    }

}
