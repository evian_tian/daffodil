package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.*;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Dict;
import com.daffodil.core.annotation.Excel;
import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 参数配置表
 * 
 * @author yweijian
 * @date 2019年8月15日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_config")
public class SysConfig extends BaseEntity<String> {
	private static final long serialVersionUID = -675181048834195075L;

	/** 参数编号 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "config_id")
	@Excel(name = "参数编号")
	private String id;
	
	/** 参数名称 */
	@Column(name = "config_name")
	@Excel(name = "参数名称")
	@NotBlank(message = "参数名称不能为空")
	@Size(min = 0, max = 100, message = "参数名称不能超过100个字符")
	@Hql(type = Logical.LIKE)
	private String configName;

	/** 参数键名 */
	@Column(name = "config_key")
	@Excel(name = "参数键名")
	@NotBlank(message = "参数键名长度不能为空")
	@Size(min = 0, max = 100, message = "参数键名长度不能超过100个字符")
	@Hql(type = Logical.LIKE)
	private String configKey;

	/** 参数键值 */
	@Column(name = "config_value")
	@Excel(name = "参数键值")
	@Size(min = 0, max = 500, message = "参数键值长度不能超过500个字符")
	@Hql(type = Logical.LIKE)
	private String configValue;

	/** 系统内置（Y是 N否） */
	@Column(name = "config_type")
	@Dict(value= "sys_yes_no")
	@Excel(name = "系统内置", readConverterExp = "Y=是,N=否")
	@Hql(type = Logical.EQ)
	private String configType;
	
	/** 创建者 */
	@Column(name="create_by")
	@Hql(type = Logical.LIKE)
	private String createBy;

	/** 创建时间 */
	@Column(name="create_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/** 更新者 */
	@Column(name="update_by")
	@Hql(type = Logical.LIKE)
	private String updateBy;

	/** 更新时间 */
	@Column(name="update_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

	/** 备注 */
	@Column(name="remark")
	@Hql(type = Logical.LIKE)
	private String remark;

}
