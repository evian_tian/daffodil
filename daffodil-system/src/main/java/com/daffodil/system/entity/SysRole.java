package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Dict;
import com.daffodil.core.annotation.Excel;
import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 角色表
 * 
 * @author yweijian
 * @date 2019年12月12日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="sys_role")
public class SysRole extends BaseEntity<String> {
	private static final long serialVersionUID = 970552735364961665L;

	/** 角色ID */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "role_id")
	@Excel(name = "角色编号")
	private String id;
	
	/** 角色名称 */
	@Column(name = "role_name")
	@Excel(name = "角色名称")
	@NotBlank(message = "角色名称不能为空")
	@Size(min = 0, max = 30, message = "角色名称长度不能超过30个字符")
	@Hql(type = Logical.LIKE)
	private String roleName;

	/** 角色权限 */
	@Column(name = "role_key")
	@Excel(name = "角色权限")
	@NotBlank(message = "权限字符不能为空")
	@Size(min = 0, max = 100, message = "权限字符长度不能超过100个字符")
	@Hql(type = Logical.LIKE)
	private String roleKey;

	/** 角色排序 */
	@Column(name = "order_num")
	private Long orderNum;

	/** 数据范围（1：所有数据权限；2：自定义数据权限；3：本部门数据权限；4：本部门及以下数据权限；5：仅本用户数据权限） */
	@Column(name = "role_scope")
	@Excel(name = "数据范围", readConverterExp = "1=所有数据权限,2=自定义数据权限,3=本部门数据权限,4=本部门及以下数据权限,5=仅本用户数据权限")
	@Hql(type = Logical.EQ)
	private String dataScope;

	/** 角色状态（0正常 1停用 2删除） */
	@Column(name = "status")
	@Dict(value = "sys_data_status")
	@Excel(name = "角色状态", readConverterExp = "0=正常,1=停用,2=删除")
	@Hql(type = Logical.EQ)
	private String status;
	
	/** 创建者 */
	@Column(name="create_by")
	@Hql(type = Logical.LIKE)
	private String createBy;

	/** 创建时间 */
	@Column(name="create_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/** 更新者 */
	@Column(name="update_by")
	@Hql(type = Logical.LIKE)
	private String updateBy;

	/** 更新时间 */
	@Column(name="update_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

	/** 备注 */
	@Column(name="remark")
	@Hql(type = Logical.LIKE)
	private String remark;

	/** 菜单组 */
	@Transient
	private String[] menuIds;

	/** 部门组（数据权限） */
	@Transient
	private String[] deptIds;
	
	/** 用户是否拥有该角色，默认false */
	@Transient
	private boolean flag;
	
	@Transient
	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
}
