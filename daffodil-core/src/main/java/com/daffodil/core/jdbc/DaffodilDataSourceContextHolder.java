package com.daffodil.core.jdbc;

import java.util.ArrayList;
import java.util.List;

/**
 * 动态数据源上下文容器
 * @author yweijian
 * @date 2021年8月31日
 * @version 1.0
 * @description
 */
public class DaffodilDataSourceContextHolder {
	
	private static final ThreadLocal<String> contextHolder = new ThreadLocal<>();
	
    protected static final List<String> dataSourceLookupKeys = new ArrayList<>();
 
    public static void setDataSourceLookupKey(String dataSourceLookupKey) {
        contextHolder.set(dataSourceLookupKey);
    }
 
    public static String getDataSourceLookupKey() {
        return contextHolder.get();
    }
 
    public static void clearDataSourceLookupKey() {
        contextHolder.remove();
    }
 
    public static boolean containsDataSource(String dataSourceLookupKey){
        return dataSourceLookupKeys.contains(dataSourceLookupKey);
    }
}
