package com.daffodil.core.entity;

import java.io.Serializable;

/**
 * Entity基类
 * 
 * @author yweijian
 * @date 2019年8月15日
 * @version 1.0
 */
public class BaseEntity<ID> implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 实体编号*/
	private ID id;
	
	public ID getId() {
		return id;
	}

	public void setId(ID id) {
		this.id = id;
	}
}
