package com.daffodil.core.id;

import java.io.Serializable;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import com.daffodil.util.sequence.Sequence;

/**
 * -雪花算法ID生成器
 * 
 * @author yweijian
 * @date 2022年2月17日
 * @version 1.0
 * @description
 */
public class SnowflakeIdentifierGenerator implements IdentifierGenerator {

    private final Sequence sequence;
    
    public SnowflakeIdentifierGenerator(SequenceProperties properties) {
        super();
        sequence = new Sequence(properties.getDataCenterId(), properties.getClock(), properties.getRandomSequence());
    }

    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        return sequence.nextId();
    }

}
