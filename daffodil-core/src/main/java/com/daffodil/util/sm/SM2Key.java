package com.daffodil.util.sm;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 国密SM2
 * @author yweijian
 * @date 2020年12月16日
 * @version 1.0
 * @description
 */
@Data
@AllArgsConstructor
public class SM2Key implements Serializable{
    
    private static final long serialVersionUID = -4219779715491286255L;

    /** 私钥 */
    private String privateKey;
    
    /** 公钥 */
    private String publicKey;

}
