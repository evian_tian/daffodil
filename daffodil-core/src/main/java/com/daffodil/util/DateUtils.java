package com.daffodil.util;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间工具类
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
public class DateUtils {
	
	public static final String YYYY = "yyyy";

	public static final String YYYY_MM = "yyyy-MM";

	public static final String YYYY_MM_DD = "yyyy-MM-dd";

	public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

	public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

	/**
	 * 获取当前Date型日期
	 * 
	 * @return Date() 当前日期
	 */
	public static Date getNowDate() {
		return new Date();
	}

	/**
	 * 获取当前日期, 默认格式为yyyy-MM-dd
	 * 
	 * @return String
	 */
	public static String getDate() {
		return dateTimeNow(YYYY_MM_DD);
	}

	public static final String getTime() {
		return dateTimeNow(YYYY_MM_DD_HH_MM_SS);
	}

	public static final String dateTimeNow() {
		return dateTimeNow(YYYYMMDDHHMMSS);
	}

	public static final String dateTimeNow(final String format) {
		return parseDateToStr(format, new Date());
	}

	public static final String dateTime(final Date date) {
		return parseDateToStr(YYYY_MM_DD, date);
	}

	public static final String parseDateToStr(final String format, final Date date) {
		return new SimpleDateFormat(format).format(date);
	}

	public static final Date dateTime(final String format, final String ts) {
		try {
			return new SimpleDateFormat(format).parse(ts);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 日期路径 即年/月/日 如2018/08/08
	 */
	public static final String datePath() {
		Date now = new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy" + File.separator + "MM" + File.separator + "dd");
		return df.format(now);
	}

	/**
	 * 日期路径 即年/月/日 如20180808
	 */
	public static final String dateTime() {
		Date now = new Date();
		SimpleDateFormat df = new SimpleDateFormat(YYYY_MM_DD);
		return df.format(now);
	}

	/**
	 * 日期型字符串转化为日期 格式
	 */
	public static Date parseDate(String str) {
		if (str == null) {
			return null;
		}
		SimpleDateFormat df = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS);
		try {
			return df.parse(str);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 日期型字符串转化为日期 格式
	 */
	public static Date parseDate(String str, String format) {
		if (str == null) {
			return null;
		}
		SimpleDateFormat df = new SimpleDateFormat(format);
		try {
			return df.parse(str);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 获取服务器启动时间
	 */
	public static Date getServerStartDate() {
		long time = ManagementFactory.getRuntimeMXBean().getStartTime();
		return new Date(time);
	}

	/**
	 * 计算两个时间差
	 */
	public static String getDatePoor(Date endDate, Date nowDate) {
		long nd = 1000L * 24L * 60L * 60L;
		long nh = 1000L * 60L * 60L;
		long nm = 1000L * 60L;
		// long ns = 1000;
		// 获得两个时间的毫秒时间差异
		long diff = endDate.getTime() - nowDate.getTime();
		// 计算差多少天
		long day = diff / nd;
		// 计算差多少小时
		long hour = diff % nd / nh;
		// 计算差多少分钟
		long min = diff % nd % nh / nm;
		// 计算差多少秒//输出结果
		// long sec = diff % nd % nh % nm / ns;
		return day + "天" + hour + "小时" + min + "分钟";
	}

	/**
	 * 获取某年某月的开始日期
	 * @param year
	 * @param month
	 * @return yyyy-MM-dd
	 */
	public static String getFisrtDayOfMonth(int year,int month){
		Calendar cal = Calendar.getInstance();
		//设置年份
		cal.set(Calendar.YEAR,year);
		//设置月份
		cal.set(Calendar.MONTH, month-1);
		//获取某月最小天数
		int firstDay = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
		//设置日历中月份的最小天数
		cal.set(Calendar.DAY_OF_MONTH, firstDay);
		//格式化日期
		SimpleDateFormat df = new SimpleDateFormat(YYYY_MM_DD);
		String firstDayOfMonth = df.format(cal.getTime());
		return firstDayOfMonth;
	}

	/**
	 * 获取某年某月的结束日期 
	 * @param year
	 * @param month
	 * @return yyyy-MM-dd
	 */
	public static String getLastDayOfMonth(int year,int month){
		Calendar cal = Calendar.getInstance();
		//设置年份
		cal.set(Calendar.YEAR,year);
		//设置月份
		cal.set(Calendar.MONTH, month-1);
		//获取某月最大天数
		int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		//设置日历中月份的最大天数
		cal.set(Calendar.DAY_OF_MONTH, lastDay);
		//格式化日期
		SimpleDateFormat sdf = new SimpleDateFormat(YYYY_MM_DD);
		String lastDayOfMonth = sdf.format(cal.getTime());
		return lastDayOfMonth;
	}
	
	/**
	 * 获取某年某月的开始时间 
	 * @param year
	 * @param month
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static String getFisrtDayTimeOfMonth(int year,int month){
		return getFisrtDayOfMonth(year, month) + " 00:00:00";
	}
	
	/**
	 * 获取某年某月的结束时间 
	 * @param year
	 * @param month
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static String getLastDayTimeOfMonth(int year,int month){
		return getLastDayOfMonth(year, month) + " 23:23:59";
	}
}
