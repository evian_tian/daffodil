package com.daffodil.util;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;

/**
 * -Http请求返回消息体
 * @author yweijian
 * @date 2022年4月12日
 * @version 1.0
 * @description
 */
@Data
public class HttpResponseData {
    
    private int status;
    
    private String message;
    
    private String body;

    private Map<String, List<String>> headers = new LinkedHashMap<String, List<String>>();
    
    public void setHeaders(Map<String, List<String>> headers) {
        this.headers = new LinkedHashMap<String, List<String>>();
        if(StringUtils.isNotNull(headers)) {
            for(Map.Entry<String, List<String>> entry : headers.entrySet()) {
                if(StringUtils.isNotNull(entry.getKey())) {
                    this.headers.put(entry.getKey(), entry.getValue());
                }
            }
        }
    }
}
