package com.daffodil.util;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpMethod;

import lombok.Data;

/**
 * -Http请求返回消息体
 * @author yweijian
 * @date 2022年4月9日
 * @version 1.0
 * @description
 */
@Data
public class HttpResponseEntity {

    private HttpMethod method;
    
    private URL url;
    
    private String params;

    private String body;
    
    private Map<String, String> headers = new HashMap<String, String>();

    private HttpResponseData data;
    
    /**
     * -将请求响应返回体转成Map
     * @return
     */
    @SuppressWarnings("unchecked")
    public Map<String, Object> toHashMap() {
        Map<String, Object> body = JacksonUtils.toJavaObject(this.getBody(), Map.class, () -> {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("body", this.getBody());
            return map;
        });
        
        Map<String, Object> responseBody = JacksonUtils.toJavaObject(this.getData().getBody(), Map.class, () -> {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("body", this.getData().getBody());
            return map;
        });
        Map<String, Object> variables = JacksonUtils.toJavaObject(this, Map.class, () -> new HashMap<String, Object>());
        Map<String, Object> data = JacksonUtils.toJavaObject(this.getData(), Map.class, () -> new HashMap<String, Object>());
        data.put("body", responseBody);
        variables.put("data", data);
        variables.put("body", body);
        return variables;
    }
}
