package com.daffodil.service;

import com.daffodil.test.entity.SysNoticeTest0;
import com.daffodil.test.entity.SysNoticeTest1;
import com.daffodil.test.entity.SysNoticeTest2;

public interface ISysNoticeTest0Service {

	public void insertNotice(SysNoticeTest0 notice);
	
	public void insertNotice(SysNoticeTest1 notice);
	
	public void insertNotice(SysNoticeTest2 notice);
}
