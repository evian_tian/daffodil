package com.daffodil.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.annotation.TargetDataSource;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.service.ISysNoticeTest1Service;
import com.daffodil.test.entity.SysNoticeTest1;

@Service
public class SysNoticeTest1ServiceImpl implements ISysNoticeTest1Service {

	@Autowired
	private JpaDao<String> jpaDao;
	
	@Override
	@TargetDataSource("test1") //@TargetDataSource声明注解为test1数据库
	@Transactional
	public void insertNotice(SysNoticeTest1 notice) {
		jpaDao.save(notice);
	}

}
