package com.daffodil.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.annotation.TargetDataSource;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.service.ISysNoticeTest2Service;
import com.daffodil.test.entity.SysNoticeTest2;

@Service
public class SysNoticeTest2ServiceImpl implements ISysNoticeTest2Service {

	@Autowired
	private JpaDao<String> jpaDao;
	
	@Override
	@TargetDataSource("test2") //@TargetDataSource声明注解为test2数据库
	@Transactional
	public void insertNotice(SysNoticeTest2 notice) {
		jpaDao.save(notice);
	}

}
