package com.daffodil.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.annotation.TargetDataSource;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.service.ISysNoticeTest0Service;
import com.daffodil.test.entity.SysNoticeTest0;
import com.daffodil.test.entity.SysNoticeTest1;
import com.daffodil.test.entity.SysNoticeTest2;

@Service
public class SysNoticeTest0ServiceImpl implements ISysNoticeTest0Service {

	@Autowired
	private JpaDao<String> jpaDao;
	
	@Override
	//@TargetDataSource("primary") //数据源为默认主数据库 可以无需@TargetDataSource声明注解
	@Transactional
	public void insertNotice(SysNoticeTest0 notice) {
		jpaDao.save(notice);
	}

	@Override
	@TargetDataSource("test1")
	@Transactional
	public void insertNotice(SysNoticeTest1 notice) {
		jpaDao.save(notice);
		
	}

	@Override
	@TargetDataSource("test2")
	@Transactional
	public void insertNotice(SysNoticeTest2 notice) {
		jpaDao.save(notice);
	}

}
