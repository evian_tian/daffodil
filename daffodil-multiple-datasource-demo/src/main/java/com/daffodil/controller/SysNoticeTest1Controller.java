package com.daffodil.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.service.ISysNoticeTest1Service;
import com.daffodil.test.entity.SysNoticeTest1;

@RestController
@RequestMapping("/test1")
public class SysNoticeTest1Controller{
	
	@Autowired
	private ISysNoticeTest1Service service;

	@GetMapping("/save")
	@ResponseBody
	public JsonResult save() {
		SysNoticeTest1 notice = new SysNoticeTest1();
		notice.setNoticeTitle("Test1:" + new Date());
		service.insertNotice(notice);
		return JsonResult.success(notice);
	}
}
