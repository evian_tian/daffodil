package com.daffodil.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.service.ISysNoticeTest0Service;
import com.daffodil.test.entity.SysNoticeTest0;
import com.daffodil.test.entity.SysNoticeTest1;
import com.daffodil.test.entity.SysNoticeTest2;

@RestController
@RequestMapping("/test0")
public class SysNoticeTest0Controller{
	
	@Autowired
	private ISysNoticeTest0Service service;

	@GetMapping("/save")
	@ResponseBody
	public JsonResult save() {
		SysNoticeTest0 notice = new SysNoticeTest0();
		notice.setNoticeTitle("Test0:" + new Date());
		service.insertNotice(notice);
		return JsonResult.success(notice);
	}
	
	@GetMapping("/save1")
	@ResponseBody
	public JsonResult save1() {
		SysNoticeTest1 notice = new SysNoticeTest1();
		notice.setNoticeTitle("Test1:" + new Date());
		service.insertNotice(notice);
		return JsonResult.success(notice);
	}
	
	@GetMapping("/save2")
	@ResponseBody
	public JsonResult save2() {
		SysNoticeTest2 notice = new SysNoticeTest2();
		notice.setNoticeTitle("Test2:" + new Date());
		service.insertNotice(notice);
		return JsonResult.success(notice);
	}
}
