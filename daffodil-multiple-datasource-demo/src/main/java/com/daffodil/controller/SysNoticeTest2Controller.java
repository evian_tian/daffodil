package com.daffodil.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.service.ISysNoticeTest2Service;
import com.daffodil.test.entity.SysNoticeTest2;

@RestController
@RequestMapping("/test2")
public class SysNoticeTest2Controller{
	
	@Autowired
	private ISysNoticeTest2Service service;

	@GetMapping("/save")
	@ResponseBody
	public JsonResult save() {
		SysNoticeTest2 notice = new SysNoticeTest2();
		notice.setNoticeTitle("Test2:" + new Date());
		service.insertNotice(notice);
		return JsonResult.success(notice);
	}
}
